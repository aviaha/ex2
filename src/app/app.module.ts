import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { NavComponent } from './nav/nav.component';
import { HelpComponent } from './help/help.component';
import { ListComponent } from './list/list.component';
import { ListItemComponent } from './list-item/list-item.component';
import { Routes,RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NavComponent,
    HelpComponent,
    ListComponent,
    ListItemComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([//הגדרת הראוטים
      {path:'',component:MainComponent},
      {path:'List',component:ListComponent},
      {path:'ListItem',component:ListItemComponent},
      {path:'Help',component:HelpComponent},
      {path:'**',component:MainComponent}

    ])

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
