import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }
  toList(){
    this.router.navigate(['List'])
  }
  toHelp(){
    this.router.navigate(['Help'])
}
}
